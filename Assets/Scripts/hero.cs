﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class hero : MonoBehaviour {
	public float Speed = 3.5f;
	public AudioClip SoundTake;
	public AudioClip SoundEject;
	public AudioClip SoundDead;
	private AudioSource Source;
	private weapon Weapon;

	public void Start () {
		this.Source = this.GetComponent<AudioSource> ();
	}

	public void Update () {
		if (Input.GetKeyUp(KeyCode.Mouse0)) {
			this.Fire();
		}
		if (Input.GetKeyDown (KeyCode.Q)) {
			this.DropWeapon();
		}
		if (Input.GetKeyDown (KeyCode.K)) {
			this.Suicide();
		}
		if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W)) {
			this.AdvanceToward (0.0f, 1.0f);
		}
		if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A)) {
			this.AdvanceToward (-1.0f, 0.0f);
		}
		if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S)) {
			this.AdvanceToward (0.0f, -1.0f);
		}
		if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D)) {
			this.AdvanceToward (1.0f, 0.0f);
		}
	}

	public void OnTriggerStay2D(Collider2D other) {
		if (other.tag == "weapon") {
			if (Input.GetKeyDown(KeyCode.E)) {
				TakeWeapon(other.gameObject);
			}
		}
	}

	private void AdvanceToward (float x, float y) {
		float step = this.Speed * Time.deltaTime;
	
		this.transform.position = new Vector3 (
			this.transform.position.x + x * step,
			this.transform.position.y + y * step,
			this.transform.position.z
		);
	}

	public void OnCollisionEnter2D (Collision2D other) {
		if (other.collider.tag == "enemyMunition") {
			this.Dead();
		}
	}

	public void LookAt (Vector3 position) {
		Vector2 v_diff = position - this.transform.position;
		float atan2 = Mathf.Atan2 (v_diff.y, v_diff.x);

		this.transform.rotation = Quaternion.Euler (
			this.transform.rotation.x,
			this.transform.rotation.y,
			atan2 * Mathf.Rad2Deg + 90.0f
		);
		if (this.Weapon) {
			this.Weapon.transform.rotation = Quaternion.Euler (
				this.transform.rotation.x,
				this.transform.rotation.y,
				atan2 * Mathf.Rad2Deg
			);
		}
	}
	
	public void Dead() {
		this.Source.clip = this.SoundDead;
		this.Source.Play();
		Application.LoadLevelAdditive("GameOver");
		Destroy(this.gameObject);
	}

	public void Suicide() {
		this.Dead();
	}

	private void TakeWeapon(GameObject other) {
		if (!this.Weapon) {
			this.Weapon = other.GetComponent<weapon>();
			this.Weapon.Enemy = false;
			this.Weapon.transform.SetParent (this.transform);
			if (this.Weapon.IsFirearm()) {
				this.Source.clip = this.SoundTake;
				this.Source.Play ();
			}
		}
	}

	private void DropWeapon () {
		if (this.Weapon) {
			if (this.Weapon.IsFirearm()) {
				this.Source.clip = this.SoundEject;
				this.Source.Play ();
			}
			this.Weapon.transform.SetParent(null);
			this.Weapon.Enemy = true;
			this.Weapon = null;
		}
	}

	private void Fire() {
		if (this.Weapon) {
			this.Weapon.Fire();
		}
	}
	
	public string getWeaponName() {
		if (this.Weapon) {
			return (this.Weapon.ToString());
		} else {
			return ("");
		}
	}
	
	public string getMunitionCount() {
		if (this.Weapon) {
			if (this.Weapon.IsFirearm()) {
				return (" - (" + this.Weapon.MunitionLenght.ToString() + ")");
			} else {
				return (" - (infinity)");
			}
		} else {
			return ("");
		}
	}
}
