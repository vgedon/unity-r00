﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class outputWeaponHero : MonoBehaviour {
	public hero hero;
	private Text weapon;

	public void Start () {
		this.weapon = GetComponent<Text> ();
	}

	public void Update () {
		this.weapon.text = hero.getWeaponName () + hero.getMunitionCount();
	}
}
