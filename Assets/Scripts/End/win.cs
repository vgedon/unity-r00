﻿using UnityEngine;
using System.Collections;

public class win : MonoBehaviour {
	public AudioClip SoundWin;
	private AudioSource Source;
	
	void Start () {
		this.Source = this.GetComponent<AudioSource> ();
		this.Source.clip = this.SoundWin;
		this.Source.Play();
	}
}
