﻿using UnityEngine;
using System.Collections;

public class gameover : MonoBehaviour {
	public AudioClip SoundLoss;
	private AudioSource Source;

	void Start () {
		this.Source = this.GetComponent<AudioSource> ();
		this.Source.clip = this.SoundLoss;
		this.Source.Play();
	}
}
