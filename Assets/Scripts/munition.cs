﻿using UnityEngine;
using System.Collections;

public class munition : MonoBehaviour {
	public float Speed = 1.5f;
	private Vector3 position;

	public void Start () {
		this.position = Camera.main.ScreenToWorldPoint (Input.mousePosition);
	}

	public void Update () {
		this.transform.position = Vector3.MoveTowards (
			this.transform.position,
			this.position,
			this.Speed * Time.deltaTime
		);
		if (this.transform.position == this.position) {
			Destroy(this.gameObject);
		}
	}
}
