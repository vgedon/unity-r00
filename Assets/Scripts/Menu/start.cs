﻿using UnityEngine;
using System.Collections;

public class start : MonoBehaviour {
	public AudioClip SoundClick;
	private AudioSource Source;

	public void runLevel () {
		this.Source.Play();
		Application.LoadLevel ("Tutorial00");
	}
	
	public void Start () {
		this.Source = this.GetComponent<AudioSource> ();
		this.Source.clip = this.SoundClick;
	}

	public void Update () {
		if (Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.KeypadEnter)) {
			runLevel();
		}
	}
}
