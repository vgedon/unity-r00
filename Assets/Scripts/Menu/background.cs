﻿using UnityEngine;
using System.Collections;

public class background : MonoBehaviour {
	public Color[] Colorground;
	public float Speed = 1.0f;
	private short Index = 0;
	private UnityEngine.UI.Image Image;

	void Start () {
		this.Image = gameObject.GetComponent<UnityEngine.UI.Image> ();
	}

	void Update () {
		this.Image.color = Color.Lerp (
			this.Colorground[this.Index],
			this.Colorground[this.Colorground.Length-1 - this.Index],
			Mathf.PingPong (Time.time * this.Speed, 1.0f)
		);
	}
}
