﻿using UnityEngine;
using System.Collections;


namespace Completed
{
	public class GameManager : MonoBehaviour
	{
		
		public static GameManager instance = null;              //Static instance of GameManager which allows it to be accessed by any other script.
		private bool enemiesMoving;                             //Boolean to check if enemies are moving.
		public Enemy enemy;

		//Awake is always called before any Start functions
		void Awake()
		{
			//Check if instance already exists
			if (instance == null) {
				
				//if not, set instance to this
				instance = this;
			}
			//If instance already exists and it's not this:
			else if (instance != this) {
				
				//Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
				Destroy (gameObject);    
			}
		}
		
		
		
		//Update is called every frame.
		void Update()
		{
			
			//Start moving enemies.
			enemy.MoveEnemy ();
		}

	}
}
