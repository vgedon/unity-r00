﻿using UnityEngine;
using System.Collections;

public class door : MonoBehaviour {
	private BoxCollider2D bx;
	private Animator animator;

	// Use this for initialization
	void Start () {
		bx = GetComponent<BoxCollider2D> () as BoxCollider2D;
		animator = GetComponent<Animator>() as Animator;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnCollisionEnter2D(Collision2D coll) {
		Debug.Log ("enter door : " + coll);
		
		animator.SetTrigger("open");

		bx.isTrigger = true;
	}

	void OnTriggerExit2D(Collider2D coll){
		
		Debug.Log ("exit door : " + coll);
		animator.SetTrigger("close");
		
		bx.isTrigger = false;
	}
}
