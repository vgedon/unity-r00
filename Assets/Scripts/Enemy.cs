using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{
	private Transform player;
	public GameObject checkpoint;
	private Transform target;
	public bool pursuit = false;
	public float moveTime = 0.1f;
	public LayerMask blockingLayer;
	private BoxCollider2D boxCollider;
	private Rigidbody2D rb2D;
	private float inverseMoveTime;
	public AudioClip[] SoundDead;
	private AudioSource Source;
	public weapon Weapon;
	private bool firing = false;

	//Start overrides the virtual Start function of the base class.
	public void Start ()
	{

		this.Source = this.GetComponent<AudioSource> ();

		//Find the Player GameObject using it's tag and store a reference to its transform component.
		player = GameObject.FindGameObjectWithTag ("hero").transform;
		boxCollider = GetComponent<BoxCollider2D> ();
		rb2D = GetComponent <Rigidbody2D> ();
		inverseMoveTime = 1f / moveTime;
		if (Weapon != null)
			TakeWeapon (Weapon.gameObject);
		if (checkpoint != null) {
			target = checkpoint.transform;
			//Debug.Log ("set target to : " + target.position);
		}
	}

	public void Update(){
		DefineTarget ();
		attemptFire ();
		if (firing)
			return;
		if (target != null) {
			AttemptMove ();
		}
	}
	private void attemptFire(){
		RaycastHit2D hit;
		boxCollider.enabled = false;
		//Cast a line from start point to end point checking collision on blockingLayer.
		hit = Physics2D.Linecast (transform.position, player.position, blockingLayer);
		boxCollider.enabled = true;
		if (hit.transform.gameObject.tag == "hero") {
			firing = true;
			target = player;
			pursuit = true;
			StopAllCoroutines();
			lookForward();
			Debug.Log ("PAN");
			this.Weapon.Fire();
		}
	}
	
	private void Fire() {
		if (this.Weapon) {
			this.Weapon.Fire();
		}
	}

	private void TakeWeapon(GameObject other) {
		if (!this.Weapon) {
			this.Weapon = other.GetComponent<weapon>();
			this.Weapon.Enemy = false;
		}
	}


	private void DefineTarget(){
		if (pursuit) {
			target = player;
			Debug.Log ("new target is : " + target.position);
		} else if (checkpoint != null){
			target = checkpoint.transform;
			Vector2 start = transform.position;
			Vector2 end = target.position;
			if (start == end) {
				checkpoint = checkpoint.GetComponent<checkPoint>().nextCheckPoint;
				target = checkpoint.transform;
				Debug.Log ("new checkpoint is : " + target.position);
			}
		}

	}

	//Move returns true if it is able to move and false if not. 
	//Move takes parameters for x direction, y direction and a RaycastHit2D to check collision.
	protected bool Move (out RaycastHit2D hit)
	{
		//Debug.Log ("in move");
		Vector2 start = transform.position;
		Vector2 end = target.position;
		lookForward ();
		boxCollider.enabled = false;
		
		//Cast a line from start point to end point checking collision on blockingLayer.
		hit = Physics2D.Linecast (start, end, blockingLayer);
		boxCollider.enabled = true;
		if (hit.transform != null)
			Debug.Log (hit.transform.gameObject.tag == "door");
		//Check if anything was hit
		if(hit.transform == null || hit.transform.position == target.position || hit.transform.gameObject.tag == "door")
		{
			//If nothing was hit, start SmoothMovement co-routine passing in the Vector2 end as destination
			StartCoroutine (SmoothMovement ());
			
			//Return true to say that Move was successful
			return true;
		}
		return false;
	}

	
	//Permet a l'ennemi de regarder dans la direction ou il se deplace
	void lookForward() {
		Vector2 v_diff = target.position - this.transform.position;
		float atan2 = Mathf.Atan2 (v_diff.y, v_diff.x);
		
		this.transform.rotation = Quaternion.Euler (
			this.transform.rotation.x,
			this.transform.rotation.y,
			atan2 * Mathf.Rad2Deg + 90.0f);
	}

	//Co-routine for moving units from one space to next, takes a parameter end to specify where to move to.
	IEnumerator SmoothMovement ()
	{
		//While that distance is greater than a very small amount (Epsilon, almost zero):
		while (transform.position != target.position) {
			//Find a new position proportionally closer to the end, based on the moveTime
			Vector3 newPostion = Vector3.MoveTowards (transform.position, target.position, inverseMoveTime * Time.deltaTime);
			
			//Call MovePosition on attached Rigidbody2D and move it to the calculated position.
			rb2D.MovePosition (newPostion);
			//Debug.Log(newPostion + " / " + target.position + "("+boxCollider.OverlapPoint(target.position)+")");
			
			//Return and loop until sqrRemainingDistance is close enough to zero to end the function
			yield return null;
		}
	}

	private IEnumerator Dead () {
		this.Source.clip = this.SoundDead[Random.Range(0, this.SoundDead.Length-1)];
		this.Source.Play();
		while (this.Source.isPlaying) {
			yield return null;
		}
		Destroy(this.gameObject);
		yield return null;
	}

	public void OnCollisionEnter2D (Collision2D other) {
		if (other.collider.tag == "friendMunition") {
			StartCoroutine(this.Dead());
		}
	}
	
	
	//Override the AttemptMove function of MovingObject to include functionality needed for Enemy to skip turns.
	//See comments in MovingObject for more on how base AttemptMove function works.
	private void AttemptMove ()
	{
		//Hit will store whatever our linecast hits when Move is called.
		RaycastHit2D hit;
		//Debug.Log ("try to go to " + target.position);
		//Set canMove to true if Move was successful, false if failed.
		bool canMove = Move (out hit);
		//Check if nothing was hit by linecast
		if(canMove)
			//If nothing was hit, return and don't execute further code.
			return;
		OnCantMove ();
	}
	
	
	//MoveEnemy is called by the GameManger each turn to tell each Enemy to try to move towards the player.
	public void MoveEnemy ()
	{
		//Call the AttemptMove function and pass in the generic parameter Player, because Enemy is moving and expecting to potentially encounter a Player
		AttemptMove();
	}
	
	
	//OnCantMove is called if Enemy attempts to move into a space occupied by a Player, it overrides the OnCantMove function of MovingObject 
	//and takes a generic parameter T which we use to pass in the component we expect to encounter, in this case Player
	private void OnCantMove()
	{
		//Debug.Log ("CantMove");
		float distance = Vector2.Distance(transform.position, target.position);
		Transform nextDoor = target;
		foreach (GameObject Door in GameObject.FindGameObjectsWithTag("door")) {
			if (Vector2.Distance(transform.position, Door.transform.position) < distance){
				nextDoor = Door.transform;
			}
		}
		target = nextDoor;
		Debug.Log ("door target : " + target.position);
		StartCoroutine (SmoothMovement ());
	}
}
