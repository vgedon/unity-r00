﻿using UnityEngine;
using System.Collections;

public class cursor : MonoBehaviour {
	public Texture2D cursorTexture;
	public hero hero;
	private CursorMode cursorMode = CursorMode.Auto;

	public void Start () {
		Cursor.SetCursor(this.cursorTexture, Vector2.zero, this.cursorMode);
	}

	public void Update () {
		Vector3 position = Camera.main.ScreenToWorldPoint (Input.mousePosition);

		if (this.hero) {
			this.hero.LookAt (position);
			this.transform.position = new Vector3 (
				this.hero.transform.position.x,
				this.hero.transform.position.y,
				-10.0f
			);
		}
	}
}