﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class weapon : MonoBehaviour {
	public byte MunitionLenght;
	public AudioClip SoundFire;
	public AudioClip SoundEmpty;
	private AudioSource Source;
	public Texture2D TextureMunition;
	public Vector3 ScaleMunition;
	private Sprite SpriteMunition;
	public bool Enemy = true;
	
	public void Start () {
		this.Source = this.GetComponent<AudioSource> ();
		this.Source.clip = this.SoundFire;
		this.SpriteMunition = Sprite.Create (
			this.TextureMunition,
			new Rect(0, 0,
				this.TextureMunition.width,
				this.TextureMunition.height
			),
			new Vector2(0.5f ,0.5f),
			1
		);
	}

	public void Fire () {
		Debug.Log ("PIN PIN");
		if (this.MunitionLenght > 0) {
			this.MunitionLenght -= 1;
			if (this.MunitionLenght == 0) {
				this.Source.clip = this.SoundEmpty;
			}
			else {
				this.CreateMunition();
				this.Source.Play ();
			}
		} else if (!this.IsFirearm()) {
			this.CreateMunition();
			this.Source.Play ();
		}
	}

	private void CreateMunition () {
		GameObject munition = new GameObject("Munition");

		munition.AddComponent<munition>();
		munition.AddComponent<SpriteRenderer>().sprite = this.SpriteMunition;
		munition.transform.position = this.transform.position;
		munition.transform.rotation = this.transform.rotation;
		munition.transform.localScale = ScaleMunition;
		munition.AddComponent<BoxCollider2D>();
		munition.AddComponent<Rigidbody2D>().gravityScale = 0;
		munition.GetComponent<SpriteRenderer> ().sortingOrder = 1;
		if (this.Enemy) {
			munition.tag = "enemyMunition";
		} else {
			munition.tag = "friendMunition";
		}
	}

	public bool IsFirearm() {
		return (this.SoundEmpty != null);
	}
}
